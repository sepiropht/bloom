use actix::{Message, Handler};
use serde::{Serialize, Deserialize};
use kernel::{
    db::DbActor,
    myaccount::domain,
    error::KernelError,
};


#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FindAccounts {
    pub username: String,
}

impl Message for FindAccounts {
    type Result = Result<i64, KernelError>;
}

impl Handler<FindAccounts> for DbActor {
    type Result = Result<i64, KernelError>;

    fn handle(&mut self, msg: FindAccounts, _: &mut Self::Context) -> Self::Result {
        use kernel::db::schema::{
            kernel_accounts,
        };
        use diesel::prelude::*;


        let conn = self.pool.get()
            .map_err(|_| KernelError::R2d2)?;


        let total : i64 = kernel_accounts::dsl::kernel_accounts
             .filter(kernel_accounts::dsl::username.eq(username))
             .count()
             .get_result(&conn)?

        return Ok(total);

    }
}
