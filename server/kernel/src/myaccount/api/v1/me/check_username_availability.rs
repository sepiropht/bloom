use kernel::{
    api,
    log::macros::*,
    api::middlewares::{
        GetRequestLogger,
        GetRequestAuth,
    },
    KernelError,
};
use futures::{
    future::Future,
    future::ok,
    future::Either,
};
use actix_web::{
    web, Error, HttpRequest, HttpResponse, ResponseError,
};
use crate::{
    controllers,
    api::v1::models,
};

use rand::Rng;
use std::time::Duration;

use serde::{Deserialize};


pub fn get(username: web::Json<models::UserNameAvailability>, state: web::Data<api::State>, req: HttpRequest)
-> impl Future<Item = HttpResponse, Error = Error> {
    let logger = req.logger();
    let auth = req.request_auth();



    return Either::B(
       tokio_timer::sleep(Duration::from_millis(rng.gen_range(350, 550))).into_future()
        .from_err()
        .and_then(move |_|
            state.db
            .send(controllers::FindAccount {
                username: username.clone(),
            })
            .map_err(|_| KernelError::ActixMailbox)
            .from_err()
            .and_then(move |res| {
                match res {
                    Ok(total) => {
                        let res = models::IsUserNameAvailable{
                            is_available: total == 0,
                        let res = api::Response::data(res);
                        ok(HttpResponse::Ok().json(&res))
                    },
                    Err(err) => {
                        slog_error!(logger, "{}", err);
                        ok(err.error_response())
                    },
            }
        })
    );
}
