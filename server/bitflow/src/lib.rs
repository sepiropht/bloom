#![feature(ip)]

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate diesel_enum_derive;

pub mod validators;
pub mod api;
pub mod domain;
pub mod controllers;
pub mod reactors;
